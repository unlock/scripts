#!/bin/python3

'''
Generates a table of the metagenomic assembly mapping and binning statistics:

input:
1. binContigsFile, format: bin_name<tab>contig
2. samtools idxstats (mapped reads per contig stats)
3. samtools flagstat (total mapped read stats)
4. An identifier (string)

standard out:
ID
Reads
Mapped reads %
Assembly size
Bins
Total bin size
Binned %
Reads mapped to bins %
'''

import sys

binContigsFile = sys.argv[1]
idxstatsFile = sys.argv[2]
flagstatFile = sys.argv[3]
identifier = sys.argv[4]

# Get total reads from samtools flagstats
total_reads = int(open(flagstatFile,"r").readlines()[0].split()[0])

# Get totals bins and all contigs with in bin from binContigsFile
binContigs = set()
bins = 0
prev_bin = ""
for line in open(binContigsFile).readlines():
    if not "unbinned" in line:
        if line.split()[0] != prev_bin: bins+=1
        binContigs.add(line.strip().split()[1])    
        prev_bin = line.split()[0]

total_assembly_size = 0
mapped_reads = 0

total_bin_size = 0
mapped_bin_reads = 0

# Read through the samtools idxstats file
for line in open(idxstatsFile,"r").readlines():
    sline = line.split()
    contig = sline[0]
    contigLen = int(sline[1])
    contigReads = int(sline[2])

    total_assembly_size += contigLen
    mapped_reads += contigReads

    if contig in binContigs:
        total_bin_size += contigLen
        mapped_bin_reads += contigReads
    
mapped_reads_perc = round(mapped_reads/total_reads*100,2)
mapped_bin_reads_perc = round(mapped_bin_reads/mapped_reads*100,2)
binned_perc = round(total_bin_size/total_assembly_size*100,2)

print("ID"+"\t"+identifier)
print("Reads"+"\t"+str(total_reads))
print("Mapped reads"+"\t"+str(mapped_reads_perc)+"%")
print("Assembly size"+"\t"+str(total_assembly_size)+" bp")
print("Bins"+"\t"+str(bins))
print("Total bin size"+"\t"+str(total_bin_size)+" bp")
print("Binned"+"\t"+str(binned_perc)+"%")
print("Reads mapped to bins"+"\t"+str(mapped_bin_reads_perc)+"%")
