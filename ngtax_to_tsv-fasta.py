#!/usr/bin/python3

__author__ = "Bart Nijsse and Jasper Koehorst"
__copyright__ = "Copyright 2021, UNLOCK"
__credits__ = ["Bart Nijsse", "Jasper Koehorst"]
__license__ = "CC0"
__version__ = "1.0.0"
__status__ = "Development"

from rdflib import Graph
from Bio.Seq import Seq
import rdflib


def picrust2():
    fasta = open(identifier + ".picrust.fasta", "w")
    table = open(identifier + ".picrust.tsv", "w")

    qres = graph.query("""
      PREFIX gbol: <http://gbol.life/0.1/>
      select distinct ?fwdsha ?fwdseq ?revsha ?revseq ?value
      where { 
          ?asv a gbol:ASVSet .
          ?asv gbol:clusteredReadCount ?value .

          ?asv gbol:forwardASV ?fwd . 
          ?fwd gbol:sequence ?fwdseq .
          ?fwd gbol:sha384 ?fwdsha .

          ?asv gbol:reverseASV ?rev . 
          ?rev gbol:sequence ?revseq . 
          ?rev gbol:sha384 ?revsha .
      } ORDER BY ?fwdsha""")

    print("Extracting ASV's and counts from NGTax ttl..")
    for index, row in enumerate(qres):
        revseq = Seq(row.revseq).reverse_complement()
        # \n>{row.revsha}\n{revseq}", file=fasta)
        print(f">{index}\n{row.fwdseq}", file=fasta)
        print(f"{index}\t{row.value}", file=table)

    fasta.close()
    table.close()


def phyloseq():
    print("Phyloseq input files creation here")
    generate_asv()
    generate_tax()
    generate_seq()
    generate_met()


def generate_seq():
    qres = graph.query("""
    PREFIX gbol:<http://gbol.life/0.1/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX ssb: <http://ssb.wur.nl/>
    PREFIX unlock: <http://m-unlock.nl/ontology/>
    PREFIX ns1: <http://ssb.wur.nl/model/>
    SELECT DISTINCT ?asvid ?seq
    WHERE {
        ?sample a gbol:Sample .
        ?sample gbol:asv ?asv .
        ?asv gbol:forwardASV/gbol:sha384 ?fsha .
        ?asv gbol:reverseASV/gbol:sha384 ?rsha .
        ?asv gbol:forwardASV/gbol:sequence ?fwd .
        ?asv gbol:reverseASV/gbol:sequence ?rev .
        # Only ASV's with an assigned taxon
        ?asv gbol:assignedTaxon ?assignedTaxon .
        ?assignedTaxon gbol:taxonName ?taxon .
        ?assignedTaxon gbol:provenance ?prov .
        BIND(CONCAT(?fsha,'_',?rsha) AS ?asvid)
        BIND(CONCAT(?fwd, 'NNNN', ?rev) AS ?seq)
    }""")
    seq = open(identifier + "_seq.tsv", "w")
    for index, row in enumerate(qres):
      print(f"{row.asvid} target_subfragment="+fragment+f"\t{row.seq}", file=seq)
    seq.close()


def generate_tax():
    qres = graph.query("""
    PREFIX gbol:<http://gbol.life/0.1/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX ssb: <http://ssb.wur.nl/>
    PREFIX unlock: <http://m-unlock.nl/ontology/>
    PREFIX ns1: <http://ssb.wur.nl/model/>
    SELECT DISTINCT ?asvid ?taxon
    WHERE {
      ?sample a gbol:Sample .
      ?sample gbol:name ?sampleName .
      ?sample gbol:asv ?asv .
      ?asv gbol:forwardASV/gbol:sha384 ?fsha .
      ?asv gbol:reverseASV/gbol:sha384 ?rsha .
      ?asv gbol:readCount ?readCount .
      ?asv gbol:clusteredReadCount ?clusterCount .
      # Only ASV's with an assigned taxon
      ?asv gbol:assignedTaxon ?assignedTaxon .
      ?assignedTaxon gbol:taxonName ?taxon .
      ?assignedTaxon gbol:provenance ?provenance .
      BIND(CONCAT(?fsha,'_',?rsha) AS ?asvid)
    } ORDER BY ?asvid""")

    tax = open(identifier + "_tax.tsv", "w")
    for index, row in enumerate(qres):
      taxon = row.taxon
      taxon = '\t'.join(taxon.split(";"))
      print(f"{row.asvid}\t"+taxon, file=tax)
    tax.close()


def generate_asv():
    qres = graph.query("""
      PREFIX gbol:<http://gbol.life/0.1/>
      PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
      PREFIX ssb: <http://ssb.wur.nl/>
      PREFIX unlock: <http://m-unlock.nl/ontology/>
      PREFIX ns1: <http://ssb.wur.nl/model/>
      SELECT DISTINCT ?sample ?asvid ?readCount
      WHERE {
          ?sample a gbol:Sample .
          ?sample gbol:asv ?asv .
          ?asv gbol:forwardASV/gbol:sha384 ?fsha .
          ?asv gbol:reverseASV/gbol:sha384 ?rsha .
          ?asv gbol:readCount ?readCount .
          ?asv gbol:clusteredReadCount ?clusterCount .
          # Only ASV's with an assigned taxon (disabled at the moment)
          # ?asv gbol:assignedTaxon ?assignedTaxon .
          # ?assignedTaxon gbol:taxonName ?taxon .
          # ?assignedTaxon gbol:provenance ?provenance .
          BIND(CONCAT(?fsha,'_',?rsha) AS ?asvid)
      } ORDER BY ?asvid""")
    asv = open(identifier + "_asv.tsv", "w")
    for index, row in enumerate(qres):
        print(f"{args.identifier}\t{row.asvid}\t{row.readCount}\t", file=asv)
    asv.close()

def generate_met():
    # For each line in this file
    g = Graph()
    g.parse(metadata, format="turtle")
    met = open(identifier + "_met.tsv", "w")

    qres = g.query("""
    PREFIX gbol:<http://gbol.life/0.1/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX ssb: <http://ssb.wur.nl/>
    PREFIX ns1: <http://ssb.wur.nl/model/>
    PREFIX unlock: <http://m-unlock.nl/ontology/>
    PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
    PREFIX schema: <http://schema.org/>
    SELECT DISTINCT ?id ?predicate ?object
    WHERE {
        ?assay ?predicate ?object .
        ?assay a jerm:Assay .
        ?assay schema:identifier ?id .
        FILTER(!ISIRI(?object))
    }""")

    for row in qres:
        predicate = "assay_"+row.predicate.split("/")[-1]
        print(row.id + "\t"+predicate + f"\t{row.object}", file=met)

    qres = g.query("""
    PREFIX gbol:<http://gbol.life/0.1/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX ssb: <http://ssb.wur.nl/>
    PREFIX ns1: <http://ssb.wur.nl/model/>
    PREFIX unlock: <http://m-unlock.nl/ontology/>
    PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
    PREFIX schema: <http://schema.org/>
    SELECT DISTINCT ?id ?predicate ?object ?predicate_label
    WHERE {
        ?sample a jerm:Sample .
        ?sample ?predicate ?object .
        OPTIONAL { ?predicate rdfs:label ?predicate_label}
        ?sample jerm:hasPart ?assay .
        ?assay a jerm:Assay .
        ?assay schema:identifier ?id .
        FILTER(!ISIRI(?object))
    }""")

    for row in qres:
        predicate = "sample_" + row.predicate.split("/")[-1]
        if type(row.predicate_label) == rdflib.term.Literal:
            predicate = "sample_" + row.predicate_label.replace(" ","_") 
        print(row.id + "\t"+predicate + f"\t{row.object}", file=met)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='NGTax file generation')
    parser.add_argument('-t', '--turtle', help='NGTax 2.0 turtle file', required=True)
    parser.add_argument('-i', '--identifier', help='Sample identifier', required=True)
    parser.add_argument('-f', '--fragment', help='Fragment analysed', required=True)
    # Optional metadata file in RDF unlock format
    parser.add_argument('-m', '--metadata', help='Metadata turtle file')

    args = parser.parse_args()

    ttl = args.turtle
    identifier = args.identifier
    fragment = args.fragment
    metadata = args.metadata

    print("Loading NGTax 2.0 RDF file")

    g = Graph()
    graph = g.parse(ttl, format="turtle")

    picrust2()
    phyloseq()