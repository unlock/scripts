# Setup script to install all environments used by unlock workflows
# Can be executed through sync.sh when placed at /unlock/infrastructure/binaries/scripts

# Installs packages on the computational nodes

R_location=/unlock/infrastructure/R/library
conda_location=/unlock/infrastructure/conda
python_location=/unlock/infrastructure/python_venv

# Ensures that R can use the library folder
mkdir --parents $R_location

################################################################
# Conda environments
################################################################
# Update conda
source /root/miniconda/bin/activate
conda update -y -n base -c defaults conda

# New attempt for picrust
if [[ ! -d "$conda_location/picrust2_v2.4.2" ]]
then
    mamba create --yes --prefix $conda_location/picrust2_v2.4.2 -c bioconda -c conda-forge picrust2=2.4.2
fi

# Krona and install taxonomy
if [[ ! -d "$conda_location/krona_v2.8.1" ]]
then
    mamba create --yes --prefix $conda_location/krona_v2.8.1 -c bioconda krona krona=2.8.1
    conda activate $conda_location/krona_v2.8.1
    ktUpdateTaxonomy.sh 
    conda deactivate
fi

# Medaka
if [[ ! -d "$conda_location/medaka_v1.5.0" ]]
then
    mamba create --yes --prefix $conda_location/medaka_v1.5.0 -c bioconda -c conda-forge medaka=1.5.0
fi

# pycoQC
if [[ ! -d "$conda_location/pycoqc_v2.5.2" ]]
then
    mamba create --yes --prefix $conda_location/pycoqc_v2.5.2 -c conda-forge -c bioconda pycoqc=2.5.2
fi
# LongQC
# ?

# NanoQC
if [[ ! -d "$conda_location/nanoqc_v0.9.4" ]]
then
    mamba create --yes --prefix $conda_location/nanoqc_v0.9.4 -c conda-forge -c bioconda nanoqc=0.9.4
fi

# Busco
if [[ ! -d "$conda_location/busco_v5.2.2" ]]
then
    mamba create --yes --prefix $conda_location/busco_v5.2.2 -c conda-forge -c bioconda busco=5.2.2
fi

# GTDB-Tk
if [[ ! -d "$conda_location/gtdbtk_v1.7.0" ]]
then
    mamba create --yes --prefix $conda_location/gtdbtk_v1.7.0 -c conda-forge -c bioconda gtdbtk=1.7.0
    echo "export GTDBTK_DATA_PATH=/unlock/references/databases/GTDBTK/release202/" > $conda_location/gtdbtk_v1.7.0/etc/conda/activate.d/gtdbtk.sh
fi

# CheckM
if [[ ! -d "$conda_location/checkm_v1.1.3" ]]
then
    mamba create --yes --prefix $conda_location/checkm_v1.1.3 -c conda-forge -c bioconda checkm-genome=1.1.3 
    conda activate $conda_location/checkm_v1.1.3
    checkm data setRoot /unlock/references/databases/CheckM/
    conda deactivate
fi

# HUMAnN 3
if [[ ! -d "$conda_location/humann_v3.0.1" ]]
then
    mamba create --yes --prefix $conda_location/humann_v3.0.1 -c biobakery -c bioconda -c conda-forge humann=3.0.1
    conda activate /unlock/infrastructure/conda/humann_v3.0.1
    humann_config --update database_folders nucleotide /unlock/references/databases/HUMAnN/chocophlan
    humann_config --update database_folders utility_mapping /unlock/references/databases/HUMAnN/utility_mapping
    humann_config --update database_folders protein /unlock/references/databases/HUMAnN/uniref
    conda deactivate
fi

# KEGG - kofamscan
if [[ ! -d "$conda_location/kofamscan_v1.3.0" ]]
then
    mamba create --yes --prefix $conda_location/kofamscan_v1.3.0 -c conda-forge -c bioconda kofamscan=1.3.0
fi

# MultiQC
if [[ ! -d "$conda_location/multiqc_v1.12.0" ]]
then
    mamba create --yes --prefix $conda_location/multiqc_v1.12.0 -c conda-forge -c bioconda multiqc=1.12.0
fi

################################################################
# Pip environments
################################################################

# Python virtual environment
python3 -m venv /unlock/infrastructure/venv
# Activate venv
source /unlock/infrastructure/venv/bin/activate
# Upgrade because we can
/unlock/infrastructure/venv/bin/python3 -m pip install --upgrade pip

## Pip dependencies
python3 -m pip install wheel biom-format pysam numpy matplotlib checkm-genome gtdbtk rdflib python-irodsclient html5lib multiqc

python3 -m pip install cwltool==3.1.20211004060744
cwltool --version && python3 -m pip install html5lib

## Pip quast rc1 for 3.9 support
wget https://github.com/ablab/quast/releases/download/quast_5.1.0rc1/quast-5.1.0rc1.tar.gz
pip install quast-5.1.0rc1.tar.gz

deactivate

################################################################
# R Packages
################################################################

R -e "install.packages('docopt',dependencies=TRUE, repos='https://packagemanager.rstudio.com/all/__linux__/bionic/latest')"
Rscript /scripts/install2.R -n 10 -s -l $R_location -r https://packagemanager.rstudio.com/all/__linux__/bionic/latest data.table futile.logger ggplot2 optparse plyr readr reshape2 scales viridis yaml parallel castor