#!/bin/bash

id=$(echo $(basename $1)|sed 's/.xml//g');
mets=$(cat  $1 | grep "species metaid=" |cut -d ' ' -f 8|sed 's/..$//g'|sort|uniq|wc -l);
rxns=$(cat  $1 | grep -c 'reaction metaid=');
genes=$(cat $1 | grep 'fbc:geneProduct.*fbc:id=' | grep -vic spontaneous);
echo "$id $mets $rxns $genes";
